Source: policy-rcd-declarative
Section: admin
Priority: optional
Maintainer: Wouter Verhelst <wouter@debian.org>
Build-Depends: perl, debhelper-compat (= 13)
Standards-Version: 4.1.3
Vcs-Git: https://salsa.debian.org/wouter/policy-decl.git
Vcs-Browser: https://salsa.debian.org/wouter/policy-decl

Package: policy-rcd-declarative
Architecture: all
Depends: ${perl:Depends}, ${misc:Depends}, libre-engine-re2-perl, policy-rcd-declarative-allow-all | policy-rcd-default-policy
Description: policy-rc.d script with declarative syntax
 Debian policy states that packages providing system services need to
 start those services by default, and that the starting of the service
 should be done by way of the /usr/sbin/invoke-rc.d script. This script
 will execute a program /usr/sbin/policy-rc.d if it exists, allowing the
 local system administrator to override behaviour if wanted by creating
 a policy script accordin to the interface specified and installing it
 under the correct name. This interface is however somewhat problematic,
 as explained in https://bugs.debian.org/911290.
 .
 This package attempts to provide a solution by shipping a policy-rc.d
 script that allows system administrators to define policies in a
 declarative way through one or more configuration files, rather than
 providing one script that may be overwritten.

Package: policy-rcd-declarative-allow-all
Provides: policy-rcd-default-policy
Recommends: policy-rcd-declarative
Replaces: policy-rcd-declarative (<< 0.4)
Conflicts: policy-rcd-declarative (<< 0.4)
Architecture: all
Depends: ${misc:Depends}
Description: Permissive default policy for policy-rcd-declarative
 Debian policy states that packages providing system services need to
 start those services by default, and that the starting of the service
 should be done by way of the /usr/sbin/invoke-rc.d script. This script
 will execute a program /usr/sbin/policy-rc.d if it exists, allowing the
 local system administrator to override behaviour if wanted by creating
 a policy script accordin to the interface specified and installing it
 under the correct name. This interface is however somewhat problematic,
 as explained in https://bugs.debian.org/911290. The package
 policy-rcd-declarative attempts to remedy these issues.
 .
 This package contains a default policy for policy-rcd-declarative that
 allows all services to start. Using it without any more specific
 policies is equivalent to not installing the policy-rcd-declarative
 package at all.

Package: policy-rcd-declarative-deny-all
Provides: policy-rcd-default-policy
Recommends: policy-rcd-declarative
Architecture: all
Depends: ${misc:Depends}
Description: Blocking default policy for policy-rcd-declarative
 Debian policy states that packages providing system services need to
 start those services by default, and that the starting of the service
 should be done by way of the /usr/sbin/invoke-rc.d script. This script
 will execute a program /usr/sbin/policy-rc.d if it exists, allowing the
 local system administrator to override behaviour if wanted by creating
 a policy script accordin to the interface specified and installing it
 under the correct name. This interface is however somewhat problematic,
 as explained in https://bugs.debian.org/911290. The package
 policy-rcd-declarative attempts to remedy these issues.
 .
 This package contains a default policy for policy-rcd-declarative that
 allows no services to start.
